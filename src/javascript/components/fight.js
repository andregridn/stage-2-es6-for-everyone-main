import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    // resolve(showWinnerModal())
    const [leftHealthBar, rightHealthBar] = document.querySelectorAll('.arena___health-bar');
    const [initialLeftHealth, initialRightHealth] = [firstFighter['health'], secondFighter['health']];

    document.addEventListener('keydown', (event) => {
      if (firstFighter['health'] >= 0 && secondFighter['health'] >= 0) {

        if (event.code == controls.PlayerOneAttack) {
          getDamage(firstFighter, secondFighter);
        }
        if (event.code == controls.PlayerTwoAttack) {
          getDamage(secondFighter, firstFighter);
        }
        if (event.code == controls.PlayerOneBlock) {
          getDamage(secondFighter, firstFighter);
        }
        if (event.code == controls.PlayerTwoBlock) {
          getDamage(secondFighter, firstFighter);
        }

        leftHealthBar.style.width = `${(firstFighter['health'] * 100) / initialLeftHealth}%`;
        rightHealthBar.style.width = `${(secondFighter['health'] * 100) / initialRightHealth}%`;

      } else {
        let winner = firstFighter['health'] > secondFighter['health'] ? firstFighter : secondFighter;
        return resolve(winner);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  damage = damage < 0 ? 0 : damage;
  defender['health'] -= damage;
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  let power = fighter['attack'] * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;
  let power = fighter['defense'] * dodgeChance;
  return power;
}
