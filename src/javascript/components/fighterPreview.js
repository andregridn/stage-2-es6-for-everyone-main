import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)
  
  for (let item in fighter) {
    if (item != 'source') {
      let element = createElement({
        tagName: 'div',
        className: 'fighter-preview__info'
      });
      element.innerHTML = `<span>${item}:</span> ${fighter[item]}`;
      fighterElement.append(element);
    } else {
      let element = createElement({
        tagName: 'img',
        className: 'fighter-preview__avatar',
        attributes: {
          src: fighter[item]
        }
      });
      fighterElement.append(element);
    }
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });
  return imgElement;
}
