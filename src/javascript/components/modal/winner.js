import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal({title: 'The end!', bodyElement: `The winner is ${fighter['name']}`})
}
